# Ansible Role: Create performance report for Linux servers
Create performance report for Linux servers.  This will create 3 separate reports for CPU, MEMORY and SWAP and place them on ms2224.

## Requirements
Ansible_version: 2.6

remote server: /tmp

## Role default Variables
## Use group_vars for client specific settings
## Example Playbook
  - hosts: all
    roles:
      - perf_reports/tasks/main.yml

## License
MIT / BSD

## Author Information
This role was originally created in 2020 by Ken Guenther
